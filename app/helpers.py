from collections import defaultdict
import re

from .models import Group, Node, Graph


class FileTreatment():
    def __init__(self, file=None):
        file = file.decode()
        self.node = dict(zip(re.findall(r'node eid=.(\d+).', file), re.findall(r'>([\w ]+)</node', file)))
        self.group = dict(zip(re.findall(r'group eid=.(\d+).', file), re.findall(r'>([\w ]+)</group', file)))
        self.groupped = [_.split(' ') for _ in re.findall(r'groupped=.([\d ]+).', file)]
        self.graph = Graph.objects.create()

    def create_obj(self):

        list_eid = defaultdict(list)

        for k, v in self.node.items():
            try:
                node = Node.objects.get(eid=k, text=v)
                node.graph = self.graph
                node.save()
            except Node.DoesNotExist:
                Node.objects.create(eid=k, text=v, graph=self.graph)
            finally:
                list_eid['node'].append(k)

        for k, v in self.group.items():
            try:
                group = Group.objects.get(eid=k, text=v)
                group.graph = self.graph
                group.save()
            except Group.DoesNotExist:
                group = Group.objects.create(eid=k, text=v, graph=self.graph)
            finally:
                length = list(self.group.keys()).index(k)
                for _ in self.groupped[length]:
                    groupped_key = [k for k, v in list_eid.items() if _ in v]
                    if groupped_key[0] == 'node':
                        node = Node.objects.get(eid=_)
                        if node.parent_id:
                            other_group = Group.objects.get(
                                id=node.parent_id_id)
                            other_group.parent_id = group
                            other_group.save()
                        else:
                            node.parent_id = group
                            node.save()
                    elif groupped_key[0] == 'group':
                        child_group = Group.objects.get(eid=_)
                        if child_group.parent_id:
                            other_group = Group.objects.get(
                                id=child_group.parent_id_id)
                            other_group.parent_id = group
                            other_group.save()
                        else:
                            child_group.parent_id = group
                            child_group.save()
                    else:
                        pass

                list_eid['group'].append(k)

    def check(self):
        group = Group.objects.all()
        resp = []
        for _ in group:
            count = _.node_parent.count() + _.group_parent.count()
            if count < 2:
                _.delete()
                resp.append(_.eid)
            else:
                pass
        return [resp, self.graph.pk]
