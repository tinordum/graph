from django.db import models


class BaseModel(models.Model):
    eid = models.CharField(max_length=20, unique=True)
    text = models.CharField(max_length=50)

    class Meta:
        abstract = True


class Graph(models.Model):
    pass


class Node(BaseModel):
    parent_id = models.ForeignKey('Group', on_delete=models.SET_NULL, blank=True,
                                  null=True, related_name='node_parent')
    graph = models.ForeignKey(Graph, on_delete=models.SET_NULL, blank=True,
                                  null=True, related_name='graph_node')

    def __str__(self):
        return self.eid


class Group(BaseModel):
    parent_id = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True,
                                  null=True, related_name='group_parent')
    graph = models.ForeignKey(Graph, on_delete=models.SET_NULL, blank=True,
                              null=True, related_name='graph_group')

    def __str__(self):
        return self.eid
