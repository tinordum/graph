from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.generic import View, FormView
from django.urls import reverse

from .forms import FileUploadForm
from .helpers import FileTreatment
from .models import Graph, Node, Group


class GraphView(View):
    def get(self, request, **kwargs):
        graph_id = kwargs.get('pk')
        if graph_id:
            graph = Graph.objects.get(pk=graph_id)
            nodes = Node.objects.filter(graph=graph)
            groups = Group.objects.filter(graph=graph)
            node_relation = dict(zip([_.pk for _ in nodes], [_.parent_id_id for _ in nodes]))
            group_relation = dict(zip([_.pk for _ in groups], [_.parent_id_id for _ in groups]))
            data = {
                'nodes': nodes,
                'groups': groups,
                'node_relation': node_relation,
                'group_relation': group_relation
            }
            return render(request, 'graph.html', context=data)
        else:
            list = Graph.objects.all()
            return render(request, 'list_graphs.html', context={'list': list})


class FileUploadView(FormView):
    template_name = 'upload_file.html'
    form_class = FileUploadForm

    def form_valid(self, form):
        form = self.get_form(self.form_class)
        if form.is_valid():
            file = self.request.FILES.get('file').read()
            file_treatment = FileTreatment(file)
            file_treatment.create_obj()
            resp = file_treatment.check()
            if resp[0]:
                text = f'Group with the eid {str(resp[0])} was deleted because it had 0 or 1 relations.'
                return HttpResponse(content=text)
            else:
                return HttpResponseRedirect(reverse('graph', kwargs={'pk': resp[1]}))
