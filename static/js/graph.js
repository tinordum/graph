var Chart = echarts.init(document.getElementById('main'));
var groups = document.getElementsByTagName('group');
var nodes = document.getElementsByTagName('node');

var group_arr = [];
var node_arr = [];

function f(obj, name, arr) {
    rand_y = Math.floor((Math.random() * 80) + 800);
    rand_x = rand_y - Math.floor((Math.random() * 80) + 400);


    for (i = 0; i < obj.length; i++) {
        console.log(rand_x);
        val = parseInt(obj[i].getAttribute('id_in_db'), 10);
        arr.push({name: name + val, x: rand_x, y: rand_y});
        rand_x -= 50
    }
}

f(nodes, 'Node', node_arr);
f(groups, 'Group', group_arr);

var result = node_arr.concat(group_arr);

var groups_rel = document.getElementsByTagName('group_rel');
var nodes_rel = document.getElementsByTagName('node_rel');

var group_rel_arr = [];
var node_rel_arr = [];

function f_rel(obj, name, arr) {

    for (i = 0; i < obj.length; i++) {
        key_rel = parseInt(obj[i].getAttribute('key'), 10);
        val_rel = parseInt(obj[i].getAttribute('value'), 10);
        arr.push({source: 'Group'+val_rel, target: name+key_rel})
    }
}

f_rel(nodes_rel, 'Node', node_rel_arr);
f_rel(groups_rel, 'Group', group_rel_arr);

var result_rel = node_rel_arr.concat(group_rel_arr);

option = {
    title: {
        text: 'Graph'
    },
    tooltip: {},
    animationDurationUpdate: 1500,
    animationEasingUpdate: 'quinticInOut',
    series : [
        {
            type: 'graph',
            layout: 'none',
            symbolSize: 35,
            roam: true,
            label: {
                normal: {
                    show: true
                }
            },
            edgeSymbol: ['circle', 'arrow'],
            edgeSymbolSize: [4, 10],
            edgeLabel: {
                normal: {
                    textStyle: {
                        fontSize: 20
                    }
                }
            },
            data: result,
            links: result_rel,
            lineStyle: {
                normal: {
                    opacity: 0.9,
                    width: 2,
                    curveness: 0
                }
            }
        }
    ]
};

Chart.setOption(option);


